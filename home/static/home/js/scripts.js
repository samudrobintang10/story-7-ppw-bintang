$(document).ready(function() {
    $(".reorder-up").click(function () {
        var $accordion = $(this).closest('.card')
        var $prev = $accordion.prev('.card');
        if ($prev.length !== 0) {
            $accordion.insertBefore($prev);
        }
        return false;
    });

    $(".reorder-down").click(function () {
        var $accordion = $(this).closest('.card')
        var $next = $accordion.next('.card');
        if ($next.length !== 0) {
            $accordion.insertAfter($next);
        }
        return false;
    });
});