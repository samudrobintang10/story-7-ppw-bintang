from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
# Create your tests here.

from .views import index, accordion

class TestAccordion(TestCase):
    def test_accordion_url_page_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_accordion_func(self):
        found = resolve("/accordion/")
        self.assertEqual(found.func, accordion)

    def test_accordion_using_template(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'home/accordion.html')

    def test_accordion_is_exist(self):
        response = Client().get('/accordion/')
        self.assertContains(response, 'div class="accordion" id="accordionExample" style="margin-top: 60px;">')
        self.assertContains(response, 'Aktivitas Saat Ini')
        self.assertContains(response, 'Cerita Singkat')
        self.assertContains(response, 'Prestasi')
        self.assertContains(response, 'Organisasi / Kepanitaan')
    