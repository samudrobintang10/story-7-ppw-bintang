from django.shortcuts import render, redirect

nama_pribadi = "Bintang Samudro"

# Create your views here.
def index(request):
    response = {'name' : nama_pribadi}
    return render(request, 'home/index.html', response)

def accordion(request):
    return render(request, 'home/accordion.html')