# Generated by Django 3.1.1 on 2020-10-22 09:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_auto_20201022_1556'),
    ]

    operations = [
        migrations.RenameField(
            model_name='lesson',
            old_name='deskripsi_mata_kuliah',
            new_name='deskripsi_kegiatan',
        ),
    ]
